package com.nelson.pos.api.common

data class BaseResponse<M,D>(
    val status: M,
    val message: String?,
    val data: D
)