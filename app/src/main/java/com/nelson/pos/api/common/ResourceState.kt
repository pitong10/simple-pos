package com.nelson.pos.api.common

enum class ResourceState {
    SUCCESS, ERROR, LOADING
}