package com.nelson.pos.api.body

data class LoginBody(
    var username: String = "",
    var password: String = "",
)