package com.nelson.pos.api.common

data class AppError(val type: ErrorType, val message: String?)