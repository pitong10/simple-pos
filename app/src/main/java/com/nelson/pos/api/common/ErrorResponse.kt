package com.nelson.pos.api.common

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("status")
    val code: Int?,
    val message: String? = null
)