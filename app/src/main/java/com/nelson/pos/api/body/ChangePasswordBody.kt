package com.nelson.pos.api.body

data class ChangePasswordBody(
    var id: Int = 0,
    var old_password: String = "",
    var new_password: String = "",
)