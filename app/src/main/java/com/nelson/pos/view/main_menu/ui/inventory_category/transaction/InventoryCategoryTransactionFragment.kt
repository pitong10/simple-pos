package com.nelson.pos.view.main_menu.ui.inventory_category.transaction

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nelson.pos.R
import com.nelson.pos.api.common.Constant
import com.nelson.pos.api.common.ErrorType
import com.nelson.pos.base.BaseDialogFragment
import com.nelson.pos.databinding.FragmentInventoryCategoryTransactionBinding
import com.nelson.pos.extension.dismissLoading
import com.nelson.pos.extension.showAlertDialog
import com.nelson.pos.extension.showLoading
import com.nelson.pos.extension.toStringOrEmpty
import com.nelson.pos.model.ProductCategory
import com.nelson.pos.view.dialog.AlertCallback
import com.nelson.pos.view.main_menu.MainMenuActivity
import com.nelson.pos.viewmodel.InventoryViewModel

class InventoryCategoryTransactionFragment : BaseDialogFragment() {

    companion object {
        const val REQUEST_KEY = "CATEGORY_KEY"
    }

    private val viewModel: InventoryViewModel by viewModels {
        InventoryViewModel.Factory(
            requireActivity().application
        )
    }
    private lateinit var binding: FragmentInventoryCategoryTransactionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val category = arguments?.getParcelable<ProductCategory>("category")
        if (category != null) viewModel.setCategory(category)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentInventoryCategoryTransactionBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setFormtitle()
        setCloseButton()

        viewModel.observableProductCategory.observe(viewLifecycleOwner) {
            bindForm(it)
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) showLoading() else dismissLoading()
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            when (it.type) {
                ErrorType.LOGIN_UNAUTHORIZED -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    (requireActivity() as MainMenuActivity).logout()
                }

                else -> showAlertDialog("Operation Failed", it.message.toStringOrEmpty(), "OK", null)
            }
        }

        viewModel.onSuccessSubmit.observe(viewLifecycleOwner) {
            if (!it) return@observe

            val operationType = if (viewModel.isAddFormState) "add" else "update"
            val message = "Success $operationType inventory category!"
            showAlertDialog("Operation Success", message, "OK", object : AlertCallback {
                override fun onButtonClicked(dialog: Dialog) {
                    val bundle = Bundle()
                    bundle.putBoolean(Constant.OPERATION_STATUS, true)
                    setFragmentResult(REQUEST_KEY, bundle)
                    findNavController().navigateUp()
                }
            })

        }

        binding.btnSubmitForm.setOnClickListener {
            val name = binding.tieName.text.toString()
            val desc = binding.tieDescription.text.toString()
            viewModel.submitCategory(name, desc)
        }

    }

    private fun setFormtitle() {
        binding.tvFormTitle.text = if (viewModel.isAddFormState)
            getString(R.string.tv_title_add_category)
        else
            getString(R.string.tv_title_edit_category)
    }

    private fun bindForm(category: ProductCategory) {
        binding.tieName.setText(category.name)
        binding.tieDescription.setText(category.description)
    }
}