package com.nelson.pos.view.dialog

interface MainFabCallback{
    fun onButtonClicked()
}