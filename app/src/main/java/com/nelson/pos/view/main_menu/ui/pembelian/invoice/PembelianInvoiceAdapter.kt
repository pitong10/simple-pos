package com.nelson.pos.view.main_menu.ui.pembelian.invoice

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nelson.pos.databinding.ItemInvoiceBinding
import com.nelson.pos.extension.toIDRFormat
import com.nelson.pos.model.PembelianItem

class PembelianInvoiceAdapter(): ListAdapter<PembelianItem, PembelianInvoiceAdapter.OrderViewHolder>(
    callback
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val binding = ItemInvoiceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OrderViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class OrderViewHolder(private val binding: ItemInvoiceBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PembelianItem) = with(binding) {
            binding.tvItemName.text = item.product?.name ?: "Unknown Product"
            binding.tvItemQuantity.text = item.quantity.toString()
            val price = item.product?.sellPrice ?: 0
            binding.tvItemPrice.text = price.toIDRFormat()
        }
    }

    companion object {
        val callback = object : DiffUtil.ItemCallback<PembelianItem>() {
            override fun areItemsTheSame(oldItem: PembelianItem, newItem: PembelianItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PembelianItem, newItem: PembelianItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}