package com.nelson.pos.view.main_menu.ui.pembelian.invoice

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nelson.pos.R
import com.nelson.pos.api.common.ErrorType
import com.nelson.pos.base.BaseFragment
import com.nelson.pos.databinding.FragmentPembelianInvoiceBinding
import com.nelson.pos.extension.cancelLoading
import com.nelson.pos.extension.showAlertDialog
import com.nelson.pos.extension.showLoading
import com.nelson.pos.extension.toAppDateFormat
import com.nelson.pos.extension.toIDRFormat
import com.nelson.pos.extension.toStringOrEmpty
import com.nelson.pos.model.Pembelian
import com.nelson.pos.view.main_menu.MainMenuActivity
import com.nelson.pos.viewmodel.PembelianViewModel
import java.io.File
import java.io.FileOutputStream

class PembelianInvoiceFragment : BaseFragment(R.layout.fragment_pembelian_invoice) {

    private val viewModel: PembelianViewModel by viewModels {
        PembelianViewModel.Factory(
            requireActivity().application
        )
    }
    private lateinit var binding: FragmentPembelianInvoiceBinding
    private var adapter: PembelianInvoiceAdapter? = null
    private var code: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val id = arguments?.getInt("id")
        code = arguments?.getString("code")

        if (id == null || code == null) {
            findNavController().navigateUp()
            return
        }
        viewModel.setInvoiceData(id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentPembelianInvoiceBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val supportActionBar = (activity as AppCompatActivity?)!!.supportActionBar
        supportActionBar?.title = "Invoice Receipt Detail"

        adapter = PembelianInvoiceAdapter()

        binding.rvItems.layoutManager = LinearLayoutManager(requireContext())
        binding.rvItems.adapter = this.adapter

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) showLoading() else cancelLoading()
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            when (it.type) {
                ErrorType.LOGIN_UNAUTHORIZED -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    (requireActivity() as MainMenuActivity).logout()
                }

                else -> showAlertDialog(
                    "Operation Failed",
                    it.message.toStringOrEmpty(),
                    "OK",
                    null
                )
            }
        }

        viewModel.observablePembelian.observe(viewLifecycleOwner) {
            if (it == null) return@observe
            bindInvoiceView(it)
        }

        binding.btnPrintInvoice.setOnClickListener {
            saveAsImage()
        }

    }

    private fun bindInvoiceView(pembelian: Pembelian) {
        binding.tvInvoiceUser.text = pembelian.karyawan?.name
        binding.tvInvoiceDate.text = pembelian.date.toAppDateFormat()
        binding.tvInvoiceNumber.text = "${code.toStringOrEmpty()}-${pembelian.id}"
        binding.tvTotalPrice.text = pembelian.totalPrice.toIDRFormat()
        adapter?.submitList(pembelian.items)
    }

    private fun saveAsImage() {
        try {
            val filename = viewModel.getFilename(code.toStringOrEmpty())
            binding.layoutInvoice.isDrawingCacheEnabled = true;
            val bitmap = binding.layoutInvoice.drawingCache
            val f: File?
            val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            if (!path.exists()) {
                path.mkdirs()
            }

            f = File(path.absolutePath + "/" + filename + ".png")
            val ostream = FileOutputStream(f)
            bitmap.compress(Bitmap.CompressFormat.PNG, 10, ostream)
            ostream.close()

            showAlertDialog(
                "Success",
                "File saved in ${f.absolutePath}",
                "OK",
                null
            )

        } catch (e: Exception) {
            showAlertDialog(
                "Save Failed",
                "Failed to save invoice image, make sure you set permission to save file",
                "OK",
                null
            )
            e.printStackTrace()
        }

    }
}