package com.nelson.pos.view.dialog

import android.app.Dialog

interface AlertCallback{
    fun onButtonClicked(dialog: Dialog)
}