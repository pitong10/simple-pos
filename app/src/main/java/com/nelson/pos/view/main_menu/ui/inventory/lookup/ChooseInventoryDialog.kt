package com.nelson.pos.view.main_menu.ui.inventory.lookup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.course.sinteraopname.base.AdapterListener
import com.nelson.pos.api.common.Constant
import com.nelson.pos.api.common.ErrorType
import com.nelson.pos.api.common.ResourceState
import com.nelson.pos.base.BaseDialogFragment
import com.nelson.pos.databinding.FragmentLookupInventoryDialogBinding
import com.nelson.pos.extension.dismissLoading
import com.nelson.pos.extension.showLoading
import com.nelson.pos.model.Product
import com.nelson.pos.view.main_menu.MainMenuActivity
import com.nelson.pos.view.main_menu.ui.inventory.InventoryAdapter
import com.nelson.pos.viewmodel.InventoryViewModel

class ChooseInventoryDialog : BaseDialogFragment() {

    companion object {
        const val REQUEST_KEY = "SELECTED_PRODUCT_KEY"
    }

    private val viewModel: InventoryViewModel by viewModels {
        InventoryViewModel.Factory(
            requireActivity().application
        )
    }

    private lateinit var binding: FragmentLookupInventoryDialogBinding
    private var inventoryAdapter: InventoryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLookupInventoryDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setCloseButton()

        viewModel.lookupInventory("")

        inventoryAdapter = InventoryAdapter()
        inventoryAdapter?.listener = object : AdapterListener<Product> {
            override fun onclick(item: Product) {
                val bundle = Bundle()
                bundle.putParcelable(Constant.SELECTED_ITEM, item)
                setFragmentResult(REQUEST_KEY, bundle)
                findNavController().navigateUp()
            }

        }

        binding.rvInventory.layoutManager = LinearLayoutManager(context)
        binding.rvInventory.adapter = inventoryAdapter

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) showLoading() else dismissLoading()
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            when (it.type) {
                ErrorType.LOGIN_UNAUTHORIZED -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    (requireActivity() as MainMenuActivity).logout()
                }

                ErrorType.INVENTORY_NOT_FOUND -> {
                    setEmptyState(true)
                }

                else -> Toast.makeText(requireContext(), it.message.toString(), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        viewModel.observableProducts.observe(viewLifecycleOwner) {
            if (it.status == ResourceState.SUCCESS) {
                setEmptyState(it.data?.isEmpty() ?: false)
                inventoryAdapter?.submitList(it.data)
            }
        }

        binding.etSearchInventory.addTextChangedListener {
            viewModel.searchHandler.postDelayed({
                searchByUserQuery()
            }, viewModel.TYPING_DELAY)
        }

        binding.btnSearch.setOnClickListener {
            searchByUserQuery()
        }
    }

    private fun searchByUserQuery() {
        val query = binding.etSearchInventory.text.toString()
        viewModel.lookupInventory(query)
    }

    private fun setEmptyState(isEmpty: Boolean) {
        if (isEmpty) {
            binding.tvNotFound.visibility = View.VISIBLE
            return
        }

        binding.tvNotFound.visibility = View.GONE
    }
}