package com.nelson.pos.analyzer

interface ScanningResultListener {
    fun onScanned(result: String)
}