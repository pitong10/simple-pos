package com.nelson.pos.database

import androidx.room.Database
import com.nelson.pos.model.ModelDatabase
import androidx.room.RoomDatabase
import com.nelson.pos.database.dao.DatabaseDao


@Database(entities = [ModelDatabase::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun databaseDao(): DatabaseDao?
}